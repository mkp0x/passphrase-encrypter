# Passphrase encrypter

Simple BIP39 passphrase encryption/decryption tool.

## Requirements

- python3

## Usage

### Encrypt
`python3 encrypt.py`

### Decrypt
`python3 decrypt.py`

## Troubleshooting

If running python in MINGW64, you may have to run `winpty python3 {encrypt|decrypt}.py`.
