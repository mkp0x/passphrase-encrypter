LETTER_A = ord('a')

key = input('Encryption key : ')

inputWords = []
outputWords = []

for i in range(24):
    userInput = input(f'word #{i + 1} : ')
    inputWords.append(userInput[:4])

i = 0
for word in inputWords:
    processedWord = '';
    for char in word:
        charIndex = ord(char) - LETTER_A
        shift = int(key[i])
        charAfterShift = (charIndex + shift) % 26
        processedWord += chr(LETTER_A + charAfterShift)
        i = (i + 1) % len(key)
    outputWords.append(processedWord)

print(outputWords)
